# Copyright (C) 2022-2023 Free Software Foundation, Inc.
#
# Author: Simon Josefsson
#
# This file is part of Guile-GnuTLS.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

manual_title = Guile binding for GnuTLS

update-copyright-env = UPDATE_COPYRIGHT_USE_INTERVALS=2

TAR_OPTIONS += --mode=go+u,go-w --mtime=$(abs_top_srcdir)/NEWS

announce_gen_args = --cksum-checksums

gendocs_options_ = -s gnutls-guile.texi

old_NEWS_hash = 2f98291df943aa41ddf3270a0cee8915

bootstrap-tools = autoconf,automake,makeinfo,libtoolize,gnulib

upload_dest_dir_ = gnutls

local-checks-to-skip = sc_GPL_version sc_error_message_uppercase	\
	sc_prohibit_have_config_h

submodule-checks =
gl_public_submodule_commit =

exclude_file_name_regexp--sc_prohibit_test_minus_ao = ^m4/guile.m4$$
exclude_file_name_regexp--sc_readme_link_copying = ^guile/modules/system/documentation/README$$
exclude_file_name_regexp--sc_readme_link_install = $(exclude_file_name_regexp--sc_readme_link_copying)
exclude_file_name_regexp--sc_fsf_postal = ^m4/guile.m4|m4/pkg.m4$$
exclude_file_name_regexp--sc_trailing_blank = ^m4/pkg.m4$$
exclude_file_name_regexp--sc_two_space_separator_in_usage = ^build-aux/gendocs.sh$$

VC_LIST_ALWAYS_EXCLUDE_REGEX = ^maint.mk|m4/lib-link.m4|m4/lib-prefix.m4|gl/top/|build-aux/gnupload$$

_makefile_at_at_check_exceptions = ' && !/AM_V/ && !/AM_DEFAULT_V/'

indent-guile:
	-$(AM_V_at)find . -name \*.scm | xargs emacs -Q --batch --eval '(mapc (lambda (file) (find-file file) (indent-region (point-min) (point-max)) (untabify (point-min) (point-max)) (delete-trailing-whitespace) (save-buffer) (kill-buffer)) command-line-args-left)' 2>&1 | grep -v '^Indenting region...'

sc_indent_guile:
	@if test -d $(srcdir)/.git				\
		&& git --version >/dev/null 2>&1			\
		&& command -v emacs > /dev/null; then \
	  $(MAKE) -s indent-guile && git diff --exit-code; \
	fi
