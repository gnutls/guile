;;; GnuTLS --- Guile bindings for GnuTLS
;;; Copyright (C) 2024 Free Software Foundation, Inc.
;;;
;;; This file is part of Guile-GnuTLS.
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;; Check that EdDSA curves do not have a Y parameter.

(use-modules (gnutls)
             (gnutls build tests)
             (ice-9 match)
             (ice-9 receive))

(run-test
 (lambda ()
   ;; Create an EdDSA key pair. Export its public key and import it
   ;; back. Export its private key and import it back. They should have the
   ;; same x parameter.
   (let ((key-pair (generate-private-key pk-algorithm/eddsa-ed25519 ecc-curve/ed25519)))
     (receive (public-crv public-x public-y)
         (public-key-export-raw-ecc (private-key->public-key key-pair '()))
       (receive (private-crv private-x private-y private-k)
           (private-key-export-raw-ecc key-pair)
         (unless (and (not public-y) (not private-y))
           (error "the y component cannot be exported for ed25519 curves"))
         (unless (and (equal? (ecc-curve->oid public-crv)
                              (ecc-curve->oid ecc-curve/ed25519))
                      (equal? (ecc-curve->oid private-crv)
                              (ecc-curve->oid ecc-curve/ed25519)))
           (error "incorrect curve name"))
         (unless (equal? public-x private-x)
           (error "inconsistent public key"))
         (receive (reimported-public-key reimported-private-key)
             (values
              (import-raw-ecc-public-key ecc-curve/ed25519
                                         public-x
                                         #f)
              (import-raw-ecc-private-key ecc-curve/ed25519
                                          private-x
                                          #f
                                          private-k))
           (receive (re-public-crv re-public-x re-public-y)
               (public-key-export-raw-ecc reimported-public-key)
             (receive (re-private-crv re-private-x re-private-y re-private-k)
                 (private-key-export-raw-ecc reimported-private-key)
               (unless (and (not re-public-y) (not re-private-y))
                 (error "the y component cannot be re-exported for ed25519 curves"))
               (unless (and (equal? (ecc-curve->oid re-public-crv)
                                    (ecc-curve->oid ecc-curve/ed25519))
                            (equal? (ecc-curve->oid re-private-crv)
                                    (ecc-curve->oid ecc-curve/ed25519)))
                 (error "incorrect curve name"))
               (unless (equal? re-public-x re-private-x)
                 (error "inconsistent reimported public key"))
               (unless (equal? re-public-x public-x)
                 (error "export/import failed"))))))))))
