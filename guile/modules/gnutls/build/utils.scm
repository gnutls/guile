;;; GnuTLS --- Guile bindings for GnuTLS.
;;; Copyright (C) 2007-2023 Free Software Foundation, Inc.
;;;
;;; This file is part of Guile-GnuTLS.
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, see <https://www.gnu.org/licenses/>.

;;; Written by Ludovic Courtès <ludo@chbouib.org>

(define-module (gnutls build utils)
  :use-module (srfi srfi-13)
  :export (scheme-symbol->c-name))

;;;
;;; Common utilities for the binding generation code.
;;;


;;;
;;; Utilities.
;;;

(define (scheme-symbol->c-name sym)
  ;; Turn SYM, a symbol denoting a Scheme name, into a string denoting a C
  ;; name.
  (string-map (lambda (chr)
                (if (eq? chr #\-) #\_ chr))
              (symbol->string sym)))


;;; Local Variables:
;;; mode: scheme
;;; coding: utf-8
;;; End:

;;; arch-tag: 56919ee1-7cce-46b9-b90f-ae6fbcfe4159
