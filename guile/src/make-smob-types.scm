;;; Help produce Guile wrappers for GnuTLS types.
;;;
;;; GnuTLS --- Guile bindings for GnuTLS.
;;; Copyright (C) 2007-2023 Free Software Foundation, Inc.
;;;
;;; This file is part of Guile-GnuTLS.
;;;
;;; This library is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public
;;; License as published by the Free Software Foundation; either
;;; version 2.1 of the License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this library; if not, see <https://www.gnu.org/licenses/>.

;;; Written by Ludovic Courtès <ludo@chbouib.org>.


(use-modules (gnutls build smobs))


;;;
;;; The program.
;;;

(define (main . args)
  (let ((port (current-output-port)))
    (for-each (lambda (type)
                (output-smob-type-definition type port)
                (output-smob-type-predicate type port))
              %gnutls-smobs)))

(apply main (cdr (command-line)))

;;; Local Variables:
;;; mode: scheme
;;; coding: utf-8
;;; End:

;;; arch-tag: 364811a0-6d0a-431a-ae50-d2f7dc529903
